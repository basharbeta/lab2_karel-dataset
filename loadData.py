import json 

#function to read file fast 
def load_file(file):
    for line in file:
        yield line

#iterate over objects of train.json
json_file = open('train.json', 'r')
for obj in load_file(json_file):
        dict = json.loads(obj)
        #here we can process each object 