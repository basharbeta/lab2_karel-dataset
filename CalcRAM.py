from pympler import asizeof
import json

json_file = open('train.json','r')
num_of_objects = 0
sum = 0
for line in json_file:
    num_of_objects += 1

json_file.seek(0)
print(asizeof.asizeof(json.loads(json_file.readline())) * num_of_objects / ( 1024 * 1024 * 1024))
