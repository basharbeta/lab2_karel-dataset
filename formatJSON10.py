import os
import json


large_file = open('train.json','r')
size = os.path.getsize('train.json')

data10 = {'data' :[]}
train10 = open('train10.json','w')
size10 = 0.1 * size
chunk = size10 / 10
for offset in range(10):
     for i in large_file.readlines(int(0.01 * size)):
          data10['data'].append(json.loads(i))
     print("chunk {}/10 is being processed".format(offset + 1))

data10['data'] = data10['data'][:-1]
print('done')

train10.write("{\"data\": ")
for i in data10['data']:
     json.dump(i, train10)
train10.write("}")
print('train10.json is ready')
